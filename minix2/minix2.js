let xRight = 10; //x cord for right side
let xLeft = 590; //x cord for left side
let speedRight = 2;
let speedLeft = 2;

function setup() {
  createCanvas(600, 600);
  print("Hello world");
  frameRate(10);
}

function draw() {
  if (keyIsPressed === true) {
  background(random(0, 20));

  //text behind emoji
  textSize(random(10, 30));
  noStroke();
  fill(255);
  text("Disgust?", random(600), random(600));
  text("Anguish?", random(600), random(600));
  text("Despair?", random(600), random(600));
  text("Averison?", random(600), random(600));
  text("Pain?", random(600), random(600));
  text("Compassion?", random(600), random(600));
  text("Empathy?", random(600), random(600));
  text("Fear?", random(600), random(600));
  text("Sadness?", random(600), random(600));

  //Base head
  stroke(1);
  fill(130, 166, 219);
  strokeWeight(7);
  ellipse(300, 330, 300, 300);
  
  //inside mouth
  noStroke();
  fill(71, 88, 160);
  rect(195, 335, 218, 75);

  //eyes
  stroke(3);
  strokeWeight(3);
  fill(255);
  ellipse(225, 310, 70, 40);
  ellipse(375, 310,70, 40);

  //teeth + tongue
  strokeWeight(2.5);
  fill(100, 119, 200)
  ellipse(300, 400, 135, 100);
  fill(255);
  rect(220, 340, 159, 40);

  //pupils
  strokeWeight(2);
  fill(0);
  ellipse(225, 320, 30, 30);
  ellipse(375, 320, 30, 30);
  
  //cheeks
  noStroke(); //eyebag coloured
  fill(130, 166, 219);
  ellipse(226, 331, 60,30);
  ellipse(385, 328, 71,20);
  ellipse(295, 355, 80, 30); 
  
  //Cover over color spillage
  ellipse(268, 356, 50, 20)
  triangle(337, 360, 337, 330, 399, 337);
  rect(195, 330, 143, 30);
  rect(222, 409.5, 153, 45);
  triangle(218, 360, 192, 390, 190, 360);
  rect(189, 380, 10, 30);
  triangle(199, 395, 228, 412, 190, 412);
  triangle(413, 334, 413, 348, 399, 337);
  triangle(360, 410, 413, 410, 413, 394);
  triangle(413, 355, 413, 394, 397, 400);
  triangle(404, 386, 413, 394, 390, 401);
  //curve under eye
  fill(130, 166, 219); 
  stroke(6);
  strokeWeight(2.5);
  curve(375, 390, 350, 328, 410, 320, 375, 390);
  curve(225, 390, 190, 325, 260, 324, 225, 390);
  
  //nose wrinkles
  curve(300, 380, 280, 320, 320, 320, 300, 380);
  curve(300, 350, 285, 305, 315, 305, 300, 350);

  //Eyebrows
  line(180, 290, 268, 270); //left eyebrows
  line(268, 270, 285, 258);
  line(420, 280, 340, 270); //right eyebrows
  line(340, 270, 320, 258);
  line(312, 266, 305, 254); //brow wrinkle
  line(305, 254, 312, 240);
  line(290, 263, 295, 255);
  line(295, 248, 275, 235);

  //Mouth
  noFill();
  beginShape();
  curveVertex(200, 380);
  curveVertex(200, 380);
  curveVertex(225, 360);
  curveVertex(295, 370);
  curveVertex(320, 365);
  curveVertex(390, 340);
  curveVertex(410, 345);
  curveVertex(410, 365);
  curveVertex(397, 393);
  curveVertex(360, 409);
  curveVertex(235, 409);
  curveVertex(202, 396);
  curveVertex(200, 380);
  curveVertex(200, 380);
  endShape();

  } else { //When key is not held

  //background for first emoji
    background(240, 200, 200);

    //text front
  noStroke();
  fill(255, 100, 100);
  textSize(30);
  text("What is this emotion?", random(160, 162), random(100, 102));
  text("Hold any key", random(205, 207), random(150, 152));
  
  //words describing emotion
  text("Pleasant?", xRight, random(300, 303));
  text("Love?", xRight, random(245, 247));
  text("Happy?", xRight, random(530, 533));
  text("Tranquil?", xRight, random(458, 461));
  text("Fine?", xLeft, random(450, 453));
  text("Satisfied?", xLeft, random(560, 563));
  text("Content?", xLeft, random(370, 373));
  text("Warm?", xLeft, random(290, 293));
  //Movement on text
  xRight = xRight + speedRight;
  xLeft = xLeft + speedLeft;
  
  if(xRight > width - 10 || xRight < 10){
    speedRight = -speedRight;
  }
  if(xLeft > width - 10 || xLeft < 10){
    speedLeft = -speedLeft;
  }
  
  //Head base #2
  stroke(1);
  strokeWeight(7);
  fill(200, 100, 100);
  ellipse(300, 330, 300, 300);
  
  //eye whites #2
  noStroke();
  fill(255);
  ellipse(225, 310, 70, 40);
  ellipse(375, 310, 70, 40);

  //pupils #2
  fill(0);
  ellipse(225, 310, 40, 50);
  ellipse(375, 310, 40, 50);
  stroke(1);
  strokeWeight(3);
  noFill();
  ellipse(225, 310, 70, 40); //eyes outline
  ellipse(375, 310, 70, 40);

  //eyebrows #2
  curve(370, 290, 413, 275, 350, 255, 370, 290);
  curve(210, 310, 190, 275, 254, 255, 210, 310);

  //mouth #2
  curve(190, 280, 220, 380, 385, 375, 280, 270);

  //pupils coverup #2
  stroke(200, 100, 100)
  strokeWeight(5.9);
  curve(190, 300, 209, 287.5, 240, 287, 280, 300);
  curve(290, 300, 360, 287, 390, 287, 420, 300);
  curve(290, 320, 360, 333, 393, 333, 480, 320);
  curve(190, 320, 209, 332.5, 240, 333, 280, 320);

  }
}