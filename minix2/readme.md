# Minix 2
![]() <img src="/minix2/screenshot.png"  width="600">
<br>
Link to: [Program](https://martatausen.gitlab.io/aesthetic-programmering/minix2/index.html)
<br>
Link to: [Source Code](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix2/minix2.js)
<br>
# What have I produced?
For this miniX I've produced an interactive program. On the start you're greeted with an emoji engulfed by warm colours and pleasant words to describe it. Over the smiling emoji there's a text suggesting you to question what kind of emotion this emoji expresses, and asks you to hold down any key. When holding down a key, you're greeted with another emoji surrounded by cold colours and an chaotic array of more negative words describing the emotion it's showing.
<br>
# What have I learnt?
This was a much more ambitious program than my first. I wanted to push further on the possibilty on making a graphical drawing using code. Emojis tend to very simplified emotions, I wanted to push it and create a emoji that conveyed a much more complicated expression that could be read in different ways. 
<br>
I found out that curves are complicated, I still don't quite understand how they work. 
The syntax looks this `curve(x1, y1, x2, y2, x3, y3, x4, y4)`
<br>
You might think that `(x1, y1)`, and `(x2, y2)` are the starting and ending point of the line, however it is actually `(x2, y2)` and `(x3, y3)`. The curve has a beginning control point which is `(x1,y2)` and an ending control point which is `(x4, y4)`. I found these really hard to understand and frustrating to work with, I ended up doing a lot guessing with the bending points.
<br>

I also explored creating my own shape, I needed a shape that could bend, so I used the syntax `curveVertex()` This one I found a bit difficult to use, but it's much easier if before you start creating your shape, you use `point()` to create little dots to guide you. I eventually deleted these from my code when I didn't need them anymore, but here's an example of how it looked, and then the actual `curveVertex()`syntax that I used, so you can compare them.
```point(200, 380);
  point(225, 360);        beginShape();  
  point(295, 370);        curveVertex(200, 380);
  point(320, 365);        curveVertex(200, 380);
  point(390, 340);        curveVertex(225, 360);
  point(410, 345);        curveVertex(295, 370);
  point(410, 365);        curveVertex(320, 365);
  point(402, 390);        curveVertex(410, 345);
  point(370, 409);        curveVertex(397, 393)
  point(228, 409);        curveVertex(360, 409);
  point(203, 395);        curveVertex(235, 409);
                          curveVertex(202, 396);
                          curveVertex(200, 380);
                          curveVertex(200, 380);
                          endShape();
  ```
 It can be hard to tell, because the actual shape needed more code lines compared to the guiding points, but you can see instances where the numbers allign completely in the parametres of both the `point()` and `curveVertex()`.
 <br>

 There were instances where colour or a shape would spill onto another, for example the curve overlaping the eyes, would have the eye shape spill under the curve, so I would cover it up with other shapes that I coloured the same as the skin and added `noStroke` to it. I also had to do this with the mouth, because I needed the teeth and tongue to be visible without spilling out of the mouth.
<br>

To make the program interactive I used `if (keyIsPressed === true) {` and the following code all includes the blue emoji and its background elements, whilst the  `else` statement includes all of the red emoji.
<br>
For the chaotic movement of the text with the blue emoji, I made it so that each text box would load in a random spot on the canvas, I did this by making the text's coordinates this: `random(600)` in both x and y. Because the canvas is 600 to 600, this would mean it can be loaded in any spot, and because the text is under draw(); it's being looped constantly while the `keyIsPressed`condition is fullfilled.
<br>

To make the text on the red scene move back and forth, I had to create variables. Here I had `xRight` and `xLeft`, which are the coordinates I want the text to start at. So all the text that has `xRight` in the x-cord, has a starting point at 10, while all the text with `xLeft` in the x-cord has a starting point at 590. Then I created variables for the speed they would move. Here is the code that's creating the movement:
 ```
 xRight = xRight + speedRight;
  xLeft = xLeft + speedLeft;
  
  if(xRight > width - 10 || xRight < 10){
    speedRight = -speedRight;
  }
  if(xLeft > width - 10 || xLeft < 10){
    speedLeft = -speedLeft;
  }
   ```
First I'm assigning the variable `xRight` to be `xRight + speedRight` and I do the same with the left ones. This makes sure that `xRight` and `xLeft` is constantly adding the number value the variables `speedRight` and `speedLeft` hold to themselves. Next we have an if statement. Where I set the condition that if `xRight` is larger than the width of the canvas - 10, then it holds the value `speedRight`, this makes the text box move forward. Then we have an OR in between, meaning that if `xRight` is smaller than 10, then it starts substracting with `-speedRight`, and this is what causes it to move backwards again.

# Reflection
Emojis have become a part of daily life for most people. At the core their purpose are to help describe feelings that can't be conveyed through text. The default colour for emojis has become yellow, which is suppose to be a neutral skin tone, however it has been very white coded, and the yellow emojis can often be percieved as being white. Although we now have skin modifers, it has functioned much more like a bandaid, in that human emojis that clearly represent a white person have just been slapped with a darker tone possibilty, and doesn't really take in account other cultures. It has gotten better although, as now there are more emoji with different types of hairstyles that aren't just straight hair. The modifers do also raise the issue of forcing people to take a decision of what skin tone they want to represent themselves, which can lead to some awkward situations. I think it's good that we have represenation of both female and male workers and as well an neutral gender for people who fall out of the binary compared to the start which where you had for example only a male police officer, which has the implication that women can't be officers.  
<br>

For my emojis, I chose to stray away from the default yellow, as I find it to be far too white coded, and I thought it would be more interesting if they were coloured more accordance to the emotion. I chose a calmer red for the first emoji, to give it more of a calming effect, red is a very warm colour, a symbol of love and passion, it can also be much more aggressive colour. For the other one, I chose blue because it's a cold colour, it can be a symbol of saddness and depression, but it can also have a calming affect like reminding us of the ocean.
<br>

Personally I find emojis tend to have an exetremly simplified expression, sometimes I don't feel like I can find the right emoji to describe how I'm feeling. Emotions are very complicated, and expressions can be extremely complicated. Emojis suggest that being happy is having your mouth wide open and grinning, while it might not look like that for everyone. I wanted to illustrate how a more complicated expression can be described in several different ways. Take the blue emoji for example, is it it a fearful expression? Or are they just disgusted by something? Maybe they're feeling empathy and grimacing in response to hearing about someone's pain? In the same way real emotions can be a mix of different emotions, sometimes someone might sound like they're crying when they're laughing or vice versa. Emojis can't fully express how we actually feel, sometimes people lie when send an emoji. They might say they're fine and send an emoji that's happy. 
