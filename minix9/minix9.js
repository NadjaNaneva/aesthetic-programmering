//Group 6 - Marta, Salomon, Kristín and Nadja
//The Tech Advocate

let statements;
let facade;
let expose = 255;
let exterior;

function preload() {
    statements = loadJSON("statements.json");
    facade = loadImage("facade.png");
    exterior = loadFont("Poppins-Regular.ttf");
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    textSize(20);
    textFont(exterior);
}

function draw() {
    image(facade, 0, 0, windowWidth, windowHeight);

    //Speechbubbles
    createStatement(300, 150);
    createStatement(250, 400);
    createStatement(300, 650);
    createStatement(750, 300);
    createStatement(730, 600);
    createStatement(1200, 150);
    createStatement(1200, 400);
    createStatement(1220, 640);

    //Whose statement?
    amazon();
    meta();
    twitter();
    tiktok();
    google();
}

function createStatement(inaccurate, statement) {
    noStroke();
    fill(255);
    ellipse(inaccurate, statement, 400, 200) //Draws speech bubble
}

function meta() {
    let privacy = 0;
    let companyValue = statements.companies[privacy].value;
    let actualFact = statements.companies[privacy].fact;
   
    //Statement 1
    if (mouseX > 90 && mouseX < 420 && mouseY > 320 && mouseY < 445) {
        background(0);
        fill(expose, 0, 0);
        textAlign(CORNER);
        text(actualFact, 80, 200, 1200, 600);
    } else {
        fill(0);
        textAlign(CENTER);
        text(companyValue, 250, 400);
    }
}

function twitter() {
    let freeSpeech = 1;
    let companyValue = statements.companies[freeSpeech].value;
    let actualFact = statements.companies[freeSpeech].fact;

    if (mouseX > 120 && mouseX < 460 && mouseY > 60 && mouseY < 225) {
        background(0);
        fill(expose, 0, 0);
        textAlign(CORNER);
        text(actualFact, 150, 190, 1200, 500);
    } else {
        fill(0);
        textAlign(CENTER);
        text(companyValue, 150, 150, 300, 200);
    }
}

function amazon() {
    let laborViolations = 2;
    let companyValue = statements.companies[laborViolations].value;
    let actualFact = statements.companies[laborViolations].fact;
    let anotherFact = statements.companies[laborViolations].secondFact;

    //Statement 1
    if (mouseX > 580 && mouseX < 900 && mouseY > 210 && mouseY < 380) {
        background(0);
        fill(expose, 0, 0);
        textAlign(CORNER);
        text(actualFact, 150, 180, 920, 700);
    } else {
        fill(0);
        textSize(20);
        textAlign(CENTER);
        text(companyValue, 560, 270, 380, 700);
    }

    //Statement 2
    if (mouseX > 1045 && mouseX < 1385 && mouseY > 315 && mouseY < 480) {
        background(0);
        fill(expose, 0, 0);
        textAlign(CORNER);
        text(anotherFact, 400, 300, 1000, 600);
    } else {
        fill(0);
        textSize(20);
        textAlign(CENTER);
        text(companyValue, 1000, 370, 390, 500);
    }
}

function tiktok() {
    let surveillance = 3;
    let companyValue = statements.companies[surveillance].value;
    let actualFact = statements.companies[surveillance].fact;
    let anotherFact = statements.companies[surveillance].secondFact;

    //Statement 1
    if (mouseX > 580 && mouseX < 875 && mouseY > 510 && mouseY < 680) {
        background(0);
        fill(expose, 0, 0);
        textAlign(CORNER);
        text(actualFact, 200, 200, 1000, 700);
    } else {
        fill(0);
        textAlign(CENTER);
        text(companyValue, 730, 600);
    }

    //Statement 2
    if (mouseX > 1030 && mouseX < 1380 && mouseY > 50 && mouseY < 230) {
        background(0);
        fill(expose, 0, 0);
        textAlign(CORNER);
        text(anotherFact, 300, 100, 900, 500);
    } else {
        fill(0);
        textAlign(CENTER);
        text(companyValue, 1200, 150);
    }
}

function google() {
    let monopoly = 4;
    let companyValue = statements.companies[monopoly].value;
    let actualFact = statements.companies[monopoly].fact;
    let anotherFact = statements.companies[monopoly].secondFact;

    //Statement 1
    if (mouseX > 145 && mouseX < 435 && mouseY > 550 && mouseY < 740) {
        background(0);
        fill(expose, 0, 0);
        textAlign(CORNER);
        text(actualFact, 120, 160, 1200, 510);
    } else {
        fill(0);
        textAlign(CENTER);
        text(companyValue, 130, 625, 330, 750);
    }

    //Statement 2
    if (mouseX > 1045 && mouseX < 1385 && mouseY > 550 && mouseY < 740) {
        background(0);
        fill(expose, 0, 0);
        textAlign(CORNER);
        text(anotherFact, 150, 200, 1200, 900);
    } else {
        fill(0);
        textAlign(CENTER);
        text(companyValue, 1040, 620, 350, 750);
    }
}

