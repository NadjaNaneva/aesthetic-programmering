function setup() {
  // put setup code here
  createCanvas(500,500);
  background(140,150,250);
  print("Hello world");
}

function draw() {
  //background element
  fill(30,0,60,5);
  rect(0,310,500,80);
  rect(0,200,500,80);
  //neck
  fill(220);
  rect(176,170,155,300);
  //body
  fill(10,0,30);
  rect(-25,425,550,300,150);
  fill(220);
  triangle(170,426,335,426,250,525);
  //ears
  ellipse(140,207,60,125);
  ellipse(358,207,60,125);
  //head
  ellipse(250,195,230,350);
  //left eye
  ellipse(195,180,50,20);
  //right eye
  ellipse(300,180,50,20);
  //left pupil
  fill(0);
  ellipse(208,180,15,15);
  //right pupil
  ellipse(313,180,15,15);
  //mouth
  fill(150,0,0);
  ellipse(250,303,60,7);
  //left eyebrow
  line(156,155,220,165);
  //right eyebrow
  line(270,165,330,155);
  //nose bridge
  line(260,168,260,245);
  //hat
  fill(0,5,20);
  rect(150,10,200,70,5);
  rect(117,75,260,60,10);
  //nose base
  fill(0);
  triangle(227,260,275,260,250,268);
}
