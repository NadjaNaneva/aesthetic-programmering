let myCanvas;
let cursor;
let img;
let raindrop = [];
let cursorSize = 150;

function preload(){
    cursor = loadImage("images/cursor1.png")
    img = loadImage("images/showerhead.png")
}

function setup(){
    myCanvas = createCanvas(windowWidth, 2000);
    myCanvas.parent("p5Wrapper");
    for (let i = 0; i < 100; i++) {
        raindrop.push(new Raindrop());
    }
    noCursor();

}

function draw(){
    push();
    clear();
    
    //shower head
    image(img, 80, -190);
    
    for (let i = 0; i < raindrop.length; i++) {
        raindrop[i].move();
        raindrop[i].show();
    }

    //cursor
    noStroke();
    imageMode(CENTER);
    image(cursor, mouseX, mouseY, cursorSize, cursorSize);
    pop();

    
    let d;
    for (let i = 0; i < raindrop.length; i++) {
    d = dist(mouseX, mouseY, raindrop[i].xpos, raindrop[i].ypos);
    if(d < cursorSize/2){
        raindrop.splice(i, 0);
    }
}
}

class Raindrop{
    constructor(){
        this.xpos = random(240, 1130);
        this.ypos = random(0, 2000);
        this.speed = random(5, 10);
    }
    move(){
        this.ypos += this.speed;
        if(this.ypos > 2000){
            this.ypos = 0;
        }
    }
    show(){
        fill(106, 191, 198);
        noStroke();
        ellipse(this.xpos, this.ypos, 10, 50);
    
        fill(156, 220, 226);
        ellipse(this.xpos, this.ypos+10, 7, 30);
        fill(255, 150);
        ellipse(this.xpos+2, this.ypos-15, 5, 10);
    }
}
