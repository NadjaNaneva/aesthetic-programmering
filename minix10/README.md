# Group members
Marta (This gitlab)
<br>
[Kristín](https://gitlab.com/kristintrang/aesthetic-programming/-/tree/main)
<br>
[Salomon](https://gitlab.com/salomonsimonsen8/aesthetic-programming/-/tree/main)
<br>
[Nadja](https://gitlab.com/NadjaNaneva/aep/-/tree/main)
<br>
# Which sample code have you chosen and why? 
We have chosen the SketchRNN model. We chose it because of how it was made. It takes data and is trained through an online drawing game from Google called Quick, Draw! It is part of machine learning research, made specifically for that. How it works is that you have 20 seconds to draw something and you get a pass when the machine can recognize what it is. If it does not recognize what you have drawn within the time limit, you lose. Also, we chose the code as it just seems fun and innocent. The drawings it produces are cute sometimes! Other times it struggles as can be seen on the screenshots:
<br>
![]() <img src="/minix10/cat8.png" width="300">
![]() <img src="/minix10/cat5.png" width="300">
<br>
And the program in action, with the model being "Swan"
![]() <img src="/minix10/swan.gif" width="400">

# Have you changed anything in the code to understand the program? Which ones?
We changed the models in the `function preload()`. We thought it would be more complicated, however, the only thing you need to do is to change the name of the model. The models are in a .js file, where they have numerous different models. We tried to put another model that is not supported, of course that failed. We played around with different parameters to understand it better, we will expand upon this in later questions.
# What lines of code have been particularly interesting to the group? Why?
The code itself looks simple. However, after taking a deeper dive into the files and a decoding session later, we have discovered that it is in fact the opposite. The code is very complicated. The most interesting part of the code is perhaps the models imported in the `function preload`. We are interested in how it loads in the models, which is part of the dataset (which we will mention numerous times). We found in the index.js file, it would load in models from a storage in www.googleapi.com.
# How would you explore/exploit the limitations of the example code or machine learning algorithms?
The most obvious is perhaps changing aspects of the code, essentially through trial-and-error. It is very easy to break the code. What we mean by this, is that it takes little for the program to stop working. We want to understand the dataset better. We cannot see how it takes the data exactly (from Google’s game) and uses it. We know it is in storage, however, we do not know exactly how it is extracted into the program.
<br>
In terms of machine learning algorithms, Google’s game falls into supervised learning as it gets input from a human and trains to recognize a pattern through those inputs. For the game, it takes all the drawings people have produced and forms a pattern to recognize what is being drawn.
<br>
# Were there syntaxes/functions that you didn't know about before? Which ones? And what did you learn?
We found that in the ml5 library itself, there were two files for SketchRNN, index.js (the main one) and models.js (holds an array of all model names). And then we focused primarily on the example code file that is to be found on the site.
We have learned alot about Javascript native symbols, such as `!` and `?` We had trouble recognizing what they meant for the code. With the help of the World Wide Web, we gathered knowledge of those symbols. `!` relates to `boolean (true/false)` and `?` is another way to write `if statements.`
<br>
Like we have mentioned before, the code itself looked simple at first glance as the syntaxes/functions are not entirely new to us. If we have to point out anything, it is perhaps in line 61 with `line(x, y, x + strokePath.dx, + y + strokePath.dy)`. The `x` and `y` are the coordinates for the first point of the line, and because `x` and `y` have been assigned to be `width/2` and `height/2` respectively, the sketch always starts in the center of the canvas. `strokePath.dx` and `strokePath.dy` is something we were not sure about in the beginning, we found that `dx` and `dy` are native attributes in javascript, they indicate a shift along the x-axis and y-axis. Meaning that here it moves the strokepath.
<br>
Continously clicking the clear button will make it extremely apparant where the point of origin of the line is:
<br>
![]() <img src="/minix10/pointoforgin.gif" width="400">
<br>
In the `function gotStoke(err, s)`, The `err` is just a placeholder because it needs a `y` value, thus needs something to be in the place of a `x` value. `s` determines what direction the stroke goes, as we found it in the index.js where `s` affected direction.
<br>
`stokePath = null;` Does nothing, taking it out and changing it to `false` or `true` does not affect the performance, as well as marking it out. We learned that `null;` is JavaScript native. It means nothing. 

# How can you see a greater relationship between the example code that you have examined and the use of machine learning out in the world (e.g. creative AI, Voice Assistance, self-driving cars, bots, facial recognition, etc.)?
We see it through the data gathered. The example code literally takes data from a machine learning database. So, for us, it is not a hidden relationship we need to dig for, it is inherently a part of it. 
<br>
What we can learn about machine code from this project is that there are many flaws in machine learning. While this may seem obvious, what we have been given here are examples and proof of this. One of them being the program's inability to draw “properly”. To specify, at certain times the program does not draw something that resembles what we had desired of it, and instead because of its fixed starting point creates something that resembles more like scribbles.
# What would you like to know more about? Or what other questions can you formulate in relation to the subject? 
In relation to both software studies and aesthetic programming, we like that it is an open-source code. However, like mentioned before, we are perhaps more interested in the data set that it uses. It is run by Google, where they explicitly say that it is for AI machine learning. We are impressed that Google is being transparent with it being an AI experiment. It makes sense with the open-source code that they would choose that data set. The dataset is stored online and the code takes straight from that. As we have said before, the dataset is perhaps the most interesting aspect.
