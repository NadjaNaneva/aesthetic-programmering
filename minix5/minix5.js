let x = 0;
let y = 0;
let spacing = 20;

///////////////////////////////////////
//Rules of the program
//Two lines are drawn at a time
//One line is either vertical or horizontal
//The other line is either diagonal forward or backwards
//The program will draw these lines horizontal until whole canvas is filled
//100 random circles are continously drawn starting at (0, 10) cords and increases down the canvas 


///////////////////////////////////////
//This function runs once
function setup(){
  createCanvas(windowWidth, windowHeight);
  background(10, 10, 20);
}

///////////////////////////////////////
//This function runs every 60fps
function draw(){
  const myRandom = random(1); //Choses a random number 0.0 to 1.0
 stroke(200, 100, 0); //Orange colour for lines
 strokeWeight(1);
 //Conditional statement for diagonal lines
 if(myRandom < 0.5){
  //If myRandom is smaller than 0.5, then it draws backslash
  line(x, y, x+spacing, y+spacing);
 }else{
  //If myRandom is bigger than 0.5, then it draws forwardslash
  line(x, y+spacing, x+spacing, y);
 }
 //Conditional statement for horizontal and verticle lines
 if(myRandom < 0.5){
  //If myRandom is smaller than 0.5, then it draws verticle line
  line(x+spacing, y+spacing, x+spacing, y);
 }else{
  //If myRandom is bigger than 0.5, then it draws horizontal line
  line(x, y+spacing, x+spacing, y+spacing);
 }
 //This controls the spacing between each line 
 x += spacing; //x = x+20; Ensures x will move 20 spaces forward
 if(x > width){ //If x reaches the end of canvas then... 
  y = y + spacing; //y should move 20 down
  x = 0; //Starting position of x
 }

 //Forloop that draws the circles
for(let i = 0; i < 100; i++){ //counts up to a 100 at a time 
    noStroke();
    fill(200, 100, 0, 70); //Alpha makes the circle fade in
    /*Circles are drawn randomly whereever x is, and starts at y+spacing: (0, 20), 
    and will increasingly increase the space it can be drawn vertically over the canvas*/
    circle(random(x), random(y+spacing), 1); 
}
}