# Welcome to my Minix3 Assignment!
![]() <img src="/minix3/screen1.png" width="800">


Link to: [Program](https://martatausen.gitlab.io/aesthetic-programmering/minix3/index.html)
<br>
Link to: [Source Code](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix3/minix3.js)
<br>
# What Have I Produced?
I've created a massive throbber that fills the entire screen, it will continously loop in a circular motion whilst a set of text will appear under the center piece of the throbber. The window can be resized and the throbber and text will always stay in the center. The text will inform the viewer that the program is loading, and will attempt to assure them that it'll be a quick process, as time goes on it becomes clearer and clearer it's just going to continue to loop with longer pauses and the program awkwardly trying to entertain the viewer while the process continues, and eventually the text will settle on "loading" for infinity.
<br>
# What Have I Learnt?
The major syntax that I explored in this minix, is the javascript native syntax `setTimeout();`. What this syntax does, is that it allows you to execute a function once after a set of time. Because it's meant for running once, I had to make sure to only use it in `setup()`. 
<br>

In order to use `setTimeout();` I needed to create functions for it execute. First I created a variable with an array of the text I wanted to appear on the screen, I named this variable `textBoxes`. 
<br>

In `setup();` I set up all the properties I wanted for the text to have, such as the font, size, colour and center aligned. Then in `draw();` I had to create the textbox itself, `text(word, width/2, height/2+120);` as you can see, where you usually would put your text in quotes, I have created a variable called word, that I have then assigned to be `word = textBoxes[0];` in the `setup()`. This will make sure that the first index in the `textBoxes` variable will be the first one to load. The cordinations of the text `width/2, height/2+120` makes sure that it stays under the center piece no matter how big your screen is.
<br>
Now that the basic stuff is set up, I could now make use of the `setTimeout();` syntax. I had to create a new function for each index in the `textBoxes` variables. Here's an example of how each new function looks.
```
function one(){
word = textBoxes[1]
} 
```
With this, I can now use `setTimeout();` to tell the program to assign word to index 1 in the array, after a set time, like this: `setTimeout(one, 5000);` the second parameter is in milliseconds, so after 5 seconds of the program running it would switch from "Loading..." to "It'll be quick, I promise". After each index has its own function like this, I could create a choreography with several `setTimeout();` placed in order.
<br>
The throbber itself, functions much like the example in chapter 3 of Aesthetic Programming. The background has a little bit of transparency to leave marks temporarily. The throbber is a custom made function, that uses `push();`and `pop();` to contain the rotation within the throbber. `translate(width/2, height/2);` moves the point of origin to the center, which then `rotate();` can be used to rotate the shapes around on. 
```
let cir = 360/num*(frameCount%num);
 rotate(radians(cir));
```
This line of code is what makes the throbber rotate in a full 360 degrees. `num` is a variable I created of how many times a shape should be drawn, and 360 is how many degrees it should be, and in `rotate(radians(cir));` the radians makes sure that the program is in degrees mode.
<br>
I have two instances of this line of code, where I have a secondary `num2` and `cir2`, where in the first set `num` is set at 50, so we get a pretty smooth circular motion due to 50 ellipses being drawn at a time. In the secondary instance, `num2` is set at 3, so we get a lot more spread out and sporadic pattern.

# Reflection
While nature ages our bodies, goes through day and night cycles, goes through four seasons and so on, our experience with time is a construct, and I wanted to explore the temporality that we experience with time in my minix. There are many factors that can drastically affect the way we perceive time, for example it's the last hour of your shift, you might become very aware of the clock, and time itself might even appear to have slowed down in that instance, even though the machine showing the time has an internal time that is consistent, 24 hours, 60 minutes, 60 seconds and repeat. 
<br>
In my minix I especially wanted to explore the slowing of time when you're met with a transition, a loading screen. Throbbers are a necessity to communicate to the user that the machine is processing something hidden underneath, without a throbber it would have just seemed like the website or video froze. Throbbers can be disruptive to the task you were under, be it you're watching a stream, and it suddenly needs to buffer and the dreaded throbber appears. A situation like that can bring frustration, and can make mere seconds feel more like minutes. When forced to wait for something we often become extremely aware of the temporality and our perception of time. In my minix while it's a massive throbber endlessly looping in the same speed, the text under it messes with our perception. At first it assures you it won't take long, and the updates in between texts aren't far, but as time drags on and the promises turn out to be untrue, the updates become longer and longer, and the text becomes more awkward and starts to small talk. This makes everything feel much longer, even though in reality in most cases it's a couple seconds in between.
