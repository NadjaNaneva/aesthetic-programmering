# DO NOT READ: Terms of Service - A Critical Design
![]() <img src="/minix4/screen1.png" width="600">
## Camera sometimes doesn't work at first click, refreshing might be neccessary!!
Link to: [Program](https://martatausen.gitlab.io/aesthetic-programmering/minix4/index.html)
<br>
Link to: [Source Code](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix4/minix4.js)
<br>
# What is DO NOT READ: Terms of Service?
DO NOT READ: the Terms of Service is a critical design meant to provoke people's tendency to ignore the Terms of Service and agreeing without much thought put into it. In this design the terms of service will discourage you from reading it fully, as it is very honest and explicit about its true intentions. It plays with the idea on how much agency we truly have in regards to our data and privacy with technology and services. You're first met with a text requesting you to agree to the Terms of Service to proceed. Around it there is a cave imagery and a shadow figure resembling "The Thinker" statue. Underneath the text box there are two buttons, one for agree and one for disagree. The agree button is coloured and has a slight gradient to draw the eye to the "appealing" option, where as the disagree button is greyed out and will run away from the cursor whenever the user attempts to click it.
<br>

After clicking agree, the user will be taken to the next scene, where the user is bombarded with different methods of data collection. Their face and voice is being recorded, every mouse click and button input are counted. There's rapid text informing the user of everything that is being collected, whilst a loading bar increases. Once this bar reaches its completion, a final scene is unveiled. Now the user is met with a bright red color, and the text thanking the user for their contribution. The Thinker-like figure is now facing the user with a menancing grin while it waves at the user in glee.

![]() <img src="/minix4/screen2.png" width="600">

# What have I learnt?
This has by far been the most ambitious code I've written so far. It has many different parts that would take far too long to explain everything, I've attempted to write explainations in the code, so hopefully that will help make clear anything I don't cover in this README. The most interesting new syntax I've used, is `switch(){` which is a statement that executes a block of code depending on different cases. Using my code as an example, I've used this to create scene changes, so I've created a global varible called `let sceneNum = O;`, and then in `draw()`, I can start my `switch statement` like this `switch(sceneNum){` now under the statement I can create cases, which will only exceute a block of code as long as the case number corresponds to the value of `sceneNum`. So, for our first scene, where we see the Terms of Service, we have `case:0` followed by everything in that scene, and then `break;` which essisentally means stop. Under this switch statement there are 3 different cases, each one of them containing the elements for each scene, followed by a break. Now to make `case:0` (scene 1) to transition to `case:1` (scene 2), I created a custom function for the Agree button:
```
function dataScene(){
  removeElements();
  userStartAudio();
  sceneNum++;
}
```
And then in `setup()` we have `button.mousePressed(dataScene);`, this ensures that the function dataScene is ran when we click on the agree button, and this custom function makes that the button disappears with `removeElements();` and the mic starts recording, and now finally `sceneNum++` takes the value of `sceneNum` and adds it with one. It goes from 0 to 1, meaning that now `case:1` is true, and this will execute all code set in that case, and remove anything in `case:0`.
<br/>
Now for the final scene change, it gets a little bit more complicated, because we have a loading bar that functions as a timer. When the loading bar reaches the end, that's when the next scene change happens.
```
rect(400, 600, loadingBar, 20); //The loading bar itself, width is set at 0.
 loadingBar += 0.3; //Loadingbar will grow with 0.3
  if (loadingBar >= 300) {  //When the loading bar's width is larger or equal to 300, sceneNum will become 2 and show scene 3
   sceneNum++ 
```
Here is the code for the loading bar itself. It has a variable called `let loadingBar = 0;`, this means because the rectangle is in the width value, it'll start at 0, and the `loadingBar` is then assigned to incease by 0.3, and finally we have a `if statement` where when the `loadingBar` is bigger or equal to 300 in width, it'll once again increase `sceneNum` moving the `case:1` (scene 2) to `case:2` (scene 3) and exceute that block of code.
<br/>

Intitally I wanted to include the facetracking points on the camera feature, but I could not figure out how to make sure the points would follow the position where the camera footage currently is. The points would be floating by themselves in the top left corner, and in the end I sadly ended up giving up including them.
<br/>
There's one more thing I would like to cover in this README, and it's the little circle under the camera to indicate it is recording camera and voice. The code looks like this:
```
blink = blink + 1 //Blink goes up by 1 everytime it's looped in draw()
  if (blink % 15 === 0){ //Everytime blink increments by 15, the condition is true.
  fill(255); //White
    circle(92, 400, 15, 15);
  } else {
  //Every other times the circle is red
    fill(255, 0, 0); //red
  circle(92, 400, 15, 15);
 ```
 What's happening here, is that I've created a variable called `blink = 0;`, and I've assigned it so when it's being looped in `draw()` it will be increased by + 1 constantly. The conditional statement that follows then checks for whenever blink reaches an increment of 15 the condition will become true, like this `15, 30, 45, 60` and so on, eveytime `blink` has these values the circle will be white, every other times the circle is red.

# Reflection
I took great inspiration from Shoshana Zuboff's Surveillence Capitalism and the documentary film by Cullen Hoback: "Terms and Conditions May Apply". We live in an age where people freely will give up information about themselves online, and most often they are blissfully unaware just how much of their own data and privacy they're giving up to big tech companies, who then can give up all that data to third parties and even authorities. While it sounds like a good thing that someone's google search and information can be used to prosecute someone or prevent someone from comitting illegal actitives, it does open up for governments to survillence on the people, and to another extent for companies to surveillence you in order to build a profile of your interests, political beliefs and so on that can then be used to adveriste specifically to you. However, the prevent aspect is much more dangerous, as we've had examples like an irish man tweeting in a joking manner about how he's ready to "destroy america" as an euphemism for he's going to party hard, being taken into interrogation for 5 hours once he landed in the airport in the US. Or a situation could arise where you might have someone who's a writer for a crime show googling search terms like "How to kill your cheating wife", "Decapitated heads" having their door knocked down for a crime they would not commit. Or the more insidious issue where the govnerment would be able to track plannings of protests and everyone contected to them, and prevent them from even happening. (Hoback, 2013)  
<br/>
Terms of Services and Privacy Policies are often purposefully written in small text and all caps to make it as un-user friendly as possible, this is done purposefully so people are more likely to unknowingly give companies allowance to sell your data. A company in the UK called Gamestation, for a single day included this in their TOS: "By placing an order via this website, you agree to a non-transferable option to claim, now and forever your immortal soul." as a joke. (Hoback, 2013) Although it was a joke, it does bring up a good point of how easily stuff can be hidden in Terms of Services.
<br/>

While my miniX has a pretty bleak outlook, where the user has no option but to agree and offers no real call to action, I hope it clearly demonstrates the "shadow" side of big tech companies. In 2019 Mark Zuckerberg held a press conference about how the "the future is private" while only a couple months later in a court case, users of Facebook were asking to have data that Facebook had sold to Cambridge Analytica released (Company that had used the data from Facebook to manipulate users towards political parties) in order to know if their data had been sold to them and used against them. The court decided that once you post something on Facebook you've relinquished all privacy. So, here we have a public front pretending to care about the user's privacy in regards to their data, whilst the underworkings are very much against it. (Zuboff, 2019)
<br/>


![]() <img src="/minix4/screen3.png" width="600">
## References
Shoshana Zuboff, 2019, “Shoshana Zuboff on Surveillance Capitalism" | VPRO Documentary
<br/>
Cullen Hoback, 2013, Terms and Conditions May Apply - documentary
