# Bottle Collector 2.0 - AWSD controls!
![]() <img src="/minix7/screen1.png" width="600">
<br/>
Link to: [Program](https://martatausen.gitlab.io/aesthetic-programmering/minix7/index.html)
<br>
Link to: [Main Source Code](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix7/minix7.js)
<br>
Link to: [Player Class file](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix7/player.js)
<br>
Link to: [Bottle Class file](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix7/bottle.js)
<br>
Link to: [Walls Class file](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix7/walls.js)
<br>
# What have I produced?
I chose to work further on the minix6, I wanted to challenge myself and see if I could create wall collisions for the bushes that previously were flat images. And well, I consider this minix both a success and a failure at completing this task, I ran into some major issues, that I don't know how to overcome, and had to scratch the maze like pattern the walls were intended to have. I added some more bottles to collect, and the wall of bushes in the middle do have collision (although it's quite broken, as you can easily get stuck if you move a speicifc way) and I added boundry to the canvas, so the player can no longer walk off.

# What have I learnt?
I certainly found out collisions are complicated, I was able to create collisions using `dist();` (that honestly worked better than what I ended up), but the major issue was that, I could create perfect collisions of a singular bush, but as soon I applied the code to more than one bush, the code would only work for the last read bush. Even though all of the bushes would register the player was touching them, when I tested it out in the console log, the actual moving the player back would only work for one bush. I tried many different methods, and this was still the case. In the end I ended up making one long rectangle, which is the bush in the middle, to demonstrate collision.
```
  if(player.xpos > this.xpos-this.w && player.xpos < this.xpos+this.w && player.ypos > this.ypos-this.h && player.ypos < this.ypos+this.h){
          player.speed = player.speed*-1; //Moves the player backwards
        }else{
          player.speed = 7; //Sets the speed back to the initial
        }
```
This is the code for that collision. It looks pretty complicated because of the length in the if statement, but what is existentially happening here, is that it checks for if the player sprite and the rectangle are overlapping at any point (Top, left, bottom, right) it will move the player backward. The `player.speed` is normally 7, so what's happening is when it overlaps it'll set the `player.speed` to `7*-1 = -7`, and when it's not overlapping it'll return back to 7, so you can move forward again. As for the canvas boundaries, it's 4 `if statements` checking if the coordinates touch the ends of the canvas, the coordinates will stay there.

# Reflections
I chose to work further on the minix6, as I had a lot of ambition to push my abilities with it, but ran out of time. I used this minix as an opportunity to reach those goals, although it did not go quite as I had hoped for it, I did learn quite a bit despite it being a bit of a failure in my opinion. I did create wall collision, but I could only manage to extend it to working on a singular object. In the minix6 I left the bushes be, because they showed pretty clearly how immersion breaking it becomes when something unrealistic happens, you would expect you couldn't walk through the bushes, but when you could and even on top of them, it reminds you that it's just flat image being processed by a machine. In this minix that is still the case, it's still just a flat image, but because there is collision that stops the player from walking through these bushes, it gives some more sense of realism to it, it becomes easier to suspend you disbelief. Although, it can quickly be broken with how easily you can become stuck in the collision. The jittering once the collision happens has a glitchy feeling to it, which then also brings us back to thinking about how the machine is processing this.
<br/>
As I've been working on these minix-es, it has become clear to me that aesthetic programming has given me an entirely different perspective to code. Now I see programming not just a problem solving method, but also as a tool for storytelling, critical thinking and great for sending a message just as any other form of art. Although writing code is technical, it does have a lot of space for individuality and many different ways to approach the same thing. I think especially my minix4 and minix6 (and minix7 as an extension) are great examples of programming can be a tool for sending a message. This minix in particular has a idyllic view to solving climate change, and functions as a call to action, but it also touches upon digital waste, in that in reality this minix probably does more harm to the climate, with the fact that it's being hosted on cloud server taking space, and the bottles you collect do not get deleted, but rather are just moved and still very much take up that space. Which of course you can parallel to the real world, where bottles might get recycled only to end back out in nature even though they were picked up.
