//Objects
let player;
let bottle = [];
let walls;

//images
let bg;
let bottles;
let bushes;
let char = [];
let flowers;
//music
let bgm;

let score = 0;

function preload() {
  char[0] = loadImage("assets/charfront.png");
  char[1] = loadImage("assets/charback.gif");
  char[2] = loadImage("assets/charleft.gif");
  char[3] = loadImage("assets/charright.gif");
  char[4] = loadImage("assets/charfront.gif");
  bottles = loadImage("assets/bottle.png");
  bg = loadImage("assets/bg.png");
  flowers = loadImage("assets/flowers.gif");
  bushes = loadImage("assets/bushes1.png");
  bgm = loadSound("assets/music.mp3");
}

function setup() {
  createCanvas(1000, 700);
  player = new Player(530, 600);
  bottle[0] = new Bottle(50, 480);
  bottle[1] = new Bottle(70, 30);
  bottle[2] = new Bottle(510, 50);
  bottle[3] = new Bottle(880, 300);
  bottle[4] = new Bottle(870, 610);
  bottle[5] = new Bottle(500, 260);
  bottle[6] = new Bottle(420, 500);
  bottle[7] = new Bottle(720, 200);
  bottle[8] = new Bottle(170, 300);

  walls = new Walls();
}

function draw() {
  background(220);
  image(bg, 0, 0);
  image(flowers, 0, 0);
  image(bushes, 0, 0);

  music();

  //Wall functions
  walls.show();
  walls.collision();

  //Player functions
  pickup();
  player.show();
  player.walk();
  player.border(); //Stops player going outside of canvas

  //Bottles collected text
  stroke(0, 100, 0);
  strokeWeight(3);
  fill(255);
  textSize(15);
  text("Bottles collected:", 10, 20)
  text(score, 127, 21);

  //Clear screen
  if (score === 9) {
    noLoop();
    fill(255);
    stroke(0, 100, 0);
    strokeWeight(4);
    textSize(50);
    textAlign(CENTER);
    text("THANK YOU FOR HELPING NATURE!", width / 2, height / 2 - 60);
    button = createButton("Help again!");
    button.position(width / 2, height / 2 - 40);
    button.size(100, 55, 30);
    button.style("background-color", "#64C167");
    button.style("color", "#ffffff");
    button.style("border-style", "dashed");
    button.style("border-color", "#ffffff");
    button.style("font-size", "20px");
    button.style("border-radius", "20px")
    button.mousePressed(refresh);
    button.mouseOver(colorChange);
    button.mouseOut(colorReset);
  }
}


////////////////////////////
//Function for playing music when "m" is pressed
function music() {
  if (keyIsDown(77)) {
    bgm.play();
  }
}

////////////////////////
// //Function for picking up a bottle
function pickup() {
  let d0;
  d0 = dist(player.xpos, player.ypos, bottle[0].xpos, bottle[0].ypos);

  if (d0 < player.w / 2) {
    bottle[0].removed();
    score++
  } else {
    bottle[0].show();
  }

  let d1;
  d1 = dist(player.xpos, player.ypos, bottle[1].xpos, bottle[1].ypos);

  if (d1 < player.w / 2) {
    bottle[1].removed();
    score++
  } else {
    bottle[1].show();
  }

  let d2;
  d2 = dist(player.xpos, player.ypos, bottle[2].xpos, bottle[2].ypos);

  if (d2 < player.w / 2) {
    bottle[2].removed();
    score++
  } else {
    bottle[2].show();
  }

  let d3;
  d3 = dist(player.xpos, player.ypos, bottle[3].xpos, bottle[3].ypos);

  if (d3 < player.w / 2) {
    bottle[3].removed();
    score++
  } else {
    bottle[3].show();
  }

  let d4;
  d4 = dist(player.xpos, player.ypos, bottle[4].xpos, bottle[4].ypos);

  if (d4 < player.w / 2) {
    bottle[4].removed();
    score++
  } else {
    bottle[4].show();
  }

  let d5;
  d5 = dist(player.xpos, player.ypos, bottle[5].xpos, bottle[5].ypos);

  if (d5 < player.w / 2) {
    bottle[5].removed();
    score++
  } else {
    bottle[5].show();
  }

  let d6;
  d6 = dist(player.xpos, player.ypos, bottle[6].xpos, bottle[6].ypos);

  if (d6 < player.w / 2) {
    bottle[6].removed();
    score++
  } else {
    bottle[6].show();
  }

  let d7;
  d7 = dist(player.xpos, player.ypos, bottle[7].xpos, bottle[7].ypos);

  if (d7 < player.w / 2) {
    bottle[7].removed();
    score++
  } else {
    bottle[7].show();
  }

  let d8;
  d8 = dist(player.xpos, player.ypos, bottle[8].xpos, bottle[8].ypos);

  if (d8 < player.w / 2) {
    bottle[8].removed();
    score++
  } else {
    bottle[8].show();
  }
}

///////////////////////////////
//Refreshes the program completely
function refresh(){
    window.location.reload();
}

//////////////////////////////
//Function for when hovering over the button
function colorChange(){
  button.style("background-color", "#A1DEA3");
}

/////////////////////////////
//Function for when mouse isn't hovering button anymore
function colorReset(){
  button.style("background-color", "#64C167"); 
}
