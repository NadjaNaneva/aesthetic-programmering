class Player {
    constructor(xpos, ypos)
    {
        this.xpos = xpos;
        this.ypos = ypos;
        this.w = 58;
        this.h = 70;
        this.speed = 7;
    }
    //This is makes the character move
    walk()
    {
        //s key DOWN
        if(keyIsDown(83)){
            this.ypos += this.speed;
        }
        //w key UP
        if(keyIsDown(87)){
            this.ypos -= this.speed;
        }
        //a key LEFT
        if(keyIsDown(65)){
            this.xpos -= this.speed;
        }
        //d key RIGHT
        if(keyIsDown(68)){
            this.xpos += this.speed;
        }
    }
    //This shows the character
    show()
    {
        //When the character is still, he will be frontfacing
       rectMode(CENTER);
        push();
        if(keyIsPressed === false){
        image(char[0], this.xpos, this.ypos, this.w, this.h);
        noStroke();
        noFill();
        }else{
            erase(); //deletes the frontfacing sprite while any key is pressed
        }
        pop();
        //s key, frontfacing sprite
        if(keyIsDown(83)){
            image(char[4], this.xpos, this.ypos, this.w, this.h); 
        }
        //w key, backfacing sprite
        if(keyIsDown(87)){
            image(char[1], this.xpos, this.ypos, this.w, this.h);
        }
        //a key, leftfacing sprite
        if(keyIsDown(65)){
            image(char[2], this.xpos, this.ypos, this.w, this.h);
        }
        //d key, rightfacing sprite
        if(keyIsDown(68)){
            image(char[3], this.xpos, this.ypos, this.w, this.h);
        }
    }
    //This stops the character from going outside of canvas
    border(){
        if(this.xpos < 0){
            this.xpos = 5;
        }
        if(this.xpos > 940){
            this.xpos = 940;
        }
        if(this.ypos < 0){
            this.ypos = 5;
        }
        if(this.ypos > 620){
            this.ypos = 620;
        }

    }
}

