class Walls {
    constructor() {
        this.xpos = 375;
        this.ypos = 385;

        this.w = 350;
        this.h = 80;
    }
    show() {
        push();
        rectMode(CORNER);
        noStroke();
        noFill(); //Hides the rectangle, so we only see the bush
        rect(this.xpos, this.ypos, this.w, this.h);
        pop();
    }
    //Creating the collision, checks for if player and wall is overlapping at any point
    collision() {
        rectMode(CENTER);
        if(player.xpos > this.xpos-this.w && player.xpos < this.xpos+this.w && player.ypos > this.ypos-this.h && player.ypos < this.ypos+this.h){
          player.speed = player.speed*-1; //Moves the player backwards
        }else{
          player.speed = 7; //Sets the speed back to the intial
        }
    }
}